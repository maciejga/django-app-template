from django.apps import AppConfig


class {{ app_name|capfirst }}Config(AppConfig):
    name = '{{ app_name }}'
    #verbose_name = "{{ app_name }}"
    #app_icon = 'md md-{{ app_name }}'